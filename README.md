# Messknoten der Haustechnik-Messanlage

Sensor- und Aktorknoten für ein dezentrales Regelungssystem.

## Plattform

Der auf der Messplatine verbaute Prozessor ist ein [STM32L051R8] der _Ultra-Low-Power_-Reihe [STM32L0] von STMicroelectronics.
Eine detaillierte Peripherie- und Toolchainbeschreibung ist im Repository haustechnik/toolchain> zu finden, daher soll an dieser Stelle auf eine nähere beschreibung der Plattform verzichtet werden.

  [STM32L051R8]: https://www.st.com/content/st_com/en/products/microcontrollers/stm32-32-bit-arm-cortex-mcus/stm32-ultra-low-power-mcus/stm32l0-series/stm32l0x1/stm32l051r8.html
  [STM32L0]: https://www.st.com/en/microcontrollers/stm32l0-series.html

## Ein- und Ausgänge

Sämtliche Ein- und Ausgänge der Platine sind nach bestem Wissen und Gewissen gegen Überspannung und EMV geschützt.
Alle IOs verfügen zunächst über eine Funkenstrecke nach Masse, dann folgt hinter einer SMD-Ferritdrossel eine TVS-Diode, ebenfalls nach Masse geschaltet.
Außerdem ist noch ein 10 nF-Kondensator als HF-Kurzschluss parallel zu der TVS-Diode geschaltet, in der Hoffnung zusammen mit dem Ferritkern ein wirksames Tiefpassfilter zu bilden.

| Port | Anzahl | Spannung | Strom | Bemerkung |
| ---- |:------:| --------:| -----:| --------- |
| DI   | 8      | 3.3 V    | wenig | potentialfrei nach Masse geschaltet und in Hardware über Tiefpassfilter und Schmitttrigger entprellt |
| DO   | 8      | 24 V     | 250 mA | masseseitig durch NMOS-Transistoren geschaltet, abgesichert über eine sich selbst zurückstellende PTC-Sicherung |
| AI   | 4      | 10 V     | wenig | über Impedanzwandler und Spannungsteiler auf 3.3 V umgesetzt |
| AO   | 4      | 10 V     | 50 mA | in Ermangelung eines DACs durch PWM mit Tiefpassfilter und anschließendem Verstärker umgesetzt |
| RI   | 8      | 2.5 V    | 1 mA | Auswertung entweder über analoge Verstärkerschaltung und ADC im Prozessor oder über [Digital-IC](/documentation/res/datasheets/ri/MAX31865.pdf) mit SPI |

## Bus

Der RS-485 Bus besitzt seine eigene, vom Restsystem unabhängige Spannungsversorgung und ist gegenüber dem Prozessor galvanisch getrennt.
Auf der Platine lässt sich durch Jumper eine 7-Bit Adresse konfigurieren, über welchen der Prozessor adressierbar ist.

## Struktur dieses Repos

| Pfad | Inhalt |
| ---- | ------ |
| [hardware/ed](hardware/ed) | KiCad-Projektordner: Schaltplan, Platine, Einstellungen |
| [hardware/lib](hardware/lib) | Bauteilbibliotheken |
| [software](software) | Projektverzeichnis CubeMX |
| [software/Src](software/Src) | Quelldateien Anwendungscode |
| [software/Inc](software/Inc) | Header-Dateien Anwendungscode |
| [software/Drivers](software/Drivers) | STM32-HAL-Treiber und ARM CMSIS-Bibliotheken |
| [software/Middlewares](software/Middlewares/Third_Party/FreeRTOS/Source) | Betriebssystem _FreeRTOS_ |
| [software/config](software/config) | Debugger-Konfiguration |
| [documentation](documentation) | Dokumentation, Notitzen, Anleitungen |
| [documentation/res](documentation/res) | Datenblätter und _Application Notes_ |
