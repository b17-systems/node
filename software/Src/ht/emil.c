#include "ht/di.h"
#include "ht/emil.h"

TaskHandle_t EmilTaskHandle;

void EmilTask(void *args);

const portPin_t emilDoMapping[] = {
    {DO1_GPIO_Port, DO1_Pin},
    {DO2_GPIO_Port, DO2_Pin},
    {DO3_GPIO_Port, DO3_Pin},
    {DO4_GPIO_Port, DO4_Pin},
    {DO5_GPIO_Port, DO5_Pin},
    {DO6_GPIO_Port, DO6_Pin}
};
 
//const portPin_t *white  = &doMapping[0];
const portPin_t *emilRed    = &emilDoMapping[1];
const portPin_t *emilYellow = &emilDoMapping[2];
//const portPin_t *green  = &doMapping[3];
const portPin_t *emilBlue   = &emilDoMapping[4];


void Emil_StartTask(void) {
    startTask(&EmilTaskHandle, EmilTask, "DI Handler", 64, 2);
}

void EmilStopTask(void) {
    stopTask(&EmilTaskHandle);
}

void EmilTask(void *args){
    diTransmitQueueEntry_t diTxEntry;
 
    while (true) {
        xQueueReceive(DITransmitQueueHandle, &diTxEntry, portMAX_DELAY);
        if (diTxEntry.ind == 0 || diTxEntry.ind == 1) {
            switch (diTxEntry.what) {
                case diLevelHigh:
                    HAL_GPIO_WritePin(emilBlue->port, emilBlue->pin, GPIO_PIN_SET);
                    break;
                case diLevelLow:
                    HAL_GPIO_WritePin(emilBlue->port, emilBlue->pin, GPIO_PIN_RESET);
                    break;
                case diActionClick:
                    HAL_GPIO_TogglePin(emilYellow->port, emilYellow->pin);
                    break;
                case diActionDClick:
                    HAL_GPIO_TogglePin(emilRed->port, emilRed->pin);
                    break;
            }
        }
    }
}

