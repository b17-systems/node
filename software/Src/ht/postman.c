#include "ht/postman.h"

#include "usart.h"

TaskHandle_t postmanTaskHandle;

void postmanTask(void *args);
void sendData(Address_t destAddr, uint8_t *data, uint8_t length);

void postman_StartTask(void) {
    startTask(&postmanTaskHandle, postmanTask, "Postman task", 64, 2);
}

void postman_StopTask(void) {
    stopTask(&postmanTaskHandle);
}

void postman_InitTask(void) {
}

void postmanTask(void *args) {
    /* if (HAL_MultiProcessor_Init(&hlpuart1, deviceAddress, UART_WAKEUPMETHOD_ADDRESSMARK) != HAL_OK)
      Error_Handler();
  
    if (HAL_MultiProcessorEx_AddressLength_Set(&hlpuart1, UART_ADDRESS_DETECT_7B) != HAL_OK)
      Error_Handler();

    if (HAL_MultiProcessor_EnableMuteMode(&hlpuart1) != HAL_OK)
      Error_Handler(); */

    //HAL_MultiProcessor_EnterMuteMode(&hlpuart1);

    bool rxne;

    while (true) {

        if (deviceAddress == 42) {
            uint32_t testNumber = 27;
            sendData(17, (uint8_t *)&testNumber, 4);
            vTaskDelay(pdMS_TO_TICKS(500));
        } else {
    
            uint8_t data[16] = {0};
            HAL_StatusTypeDef rxStatus = HAL_UART_Receive(&hlpuart1, data, 6, 1000);
            rxne = hlpuart1.Instance->ISR & USART_ISR_RXNE ? true : false;

            if (rxStatus != HAL_OK) {
               //Error_Handler();
            } else {
                uint32_t testNumber = 31415;
                sendData(42, (uint8_t *)&testNumber, 4);
                vTaskDelay(pdMS_TO_TICKS(10));
            }
            HAL_UART_StateTypeDef lpuartState = HAL_UART_GetState(&hlpuart1);
            
            vTaskDelay(pdMS_TO_TICKS(10));

            rxne = hlpuart1.Instance->ISR & USART_ISR_RXNE ? true : false;
            (void) hlpuart1.Instance->RDR;
            rxne = hlpuart1.Instance->ISR & USART_ISR_RXNE ? true : false;
        }

    }
}
void sendData(Address_t destAddr, uint8_t *data, uint8_t length) {
    uint8_t buffer[16] = {0};
    if (length > 6)
        Error_Handler();
    
    buffer[0] = destAddr;
    buffer[1] = 1;
    buffer[2] = deviceAddress;
    for (uint8_t i = 0; i < length; i++)
        buffer[4+2*i] = data[i];

    HAL_StatusTypeDef result = HAL_UART_Transmit(&hlpuart1, buffer, length + 2, 100);

    UNUSED(result);
}

