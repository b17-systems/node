#include "ht/di.h"
#include "ht/createTarget.h"

TaskHandle_t CreateTargetTaskHandle;
void CreateTargetTask(void *args);

void CreateTarget_StartTask(void) {
    startTask(&CreateTargetTaskHandle, CreateTargetTask, "DI Handler", 64, 2);
}

void CreateTarget_StopTask(void) {
    stopTask(&CreateTargetTaskHandle);
}

void CreateTargetTask(void *args){
    diTransmitQueueEntry_t diTxEntry;
 
    while (true) {
        xQueueReceive(DITransmitQueueHandle, &diTxEntry, portMAX_DELAY);
        if (diTxEntry.ind == 0 || diTxEntry.ind == 1) {
            int8_t step = 0;
            switch (diTxEntry.what) {
                case diActionClick:
                    step = diTxEntry.ind == 0 ? 1 : -1;
                    break;
                case diActionDClick:
                    step = diTxEntry.ind == 0 ? 5 : -5;
                    break;
                default:
                    break;
            }

            if (step != 0) {
                if (DOTargetValue + step > DORangeHigh) {
                    DOTargetValue = DORangeHigh;
                    DOSynch = true;
                } else if (DOTargetValue + step < DORangeLow) {
                    DOTargetValue = DORangeLow;
                    DOSynch = true;
                } else {
                    DOTargetValue += step;
                    DOSynch = false;
                }
            }
        }
    }
}