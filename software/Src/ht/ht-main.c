#include "ht/ht-main.h"
#include "ht/di.h"
#include "ht/emil.h"
#include "ht/postman.h"
#include "ht/createTarget.h"
#include "ht/stepMotor.h"


void mainTask(void *param);

TaskHandle_t MainTaskHandle = NULL;


void startMainTask(void) {
    startTask(&MainTaskHandle, mainTask, "Main Task", 64, 2);
}

void stopMainTask(void) {
    stopTask(&MainTaskHandle);
}

void startTask(TaskHandle_t *taskHandle, TaskFunction_t taskFunction, const char* taskName, uint16_t stackDepth, UBaseType_t priority) {
    if (*taskHandle == NULL) {
        // The task has not yet been created, or it was deleted again.
        // Since it does not exist, we're OK to (re)start it.
        BaseType_t taskCreateStatus = xTaskCreate(
            taskFunction, taskName, stackDepth, NULL, priority, taskHandle);

        if (taskCreateStatus == pdPASS) {
            // everything went smoothly
        } else if (taskCreateStatus == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY) {
            Error_Handler();
        } else {
            Error_Handler();
        }
        // After having been created, it will directly enter running state.
    } else {
        // The task is already existing, therefore we cannot start it here.
        // TODO: what now?
    }
}

void stopTask(TaskHandle_t *taskHandle) {
    if (*taskHandle != NULL) {
        vTaskDelete(*taskHandle);
        *taskHandle = NULL;
    }
}

uint32_t duration_ticks(void) {
    //uint32_t test = pdMS_TO_TICKS((adcBuffer[0]/* - 1000*/) / 5);
    //return test;
    return pdMS_TO_TICKS( 1000);
}

void mainTask(void *param) {
    deviceAddress = 0x7f ^ 
                   (HAL_GPIO_ReadPin(addr0_GPIO_Port, addr0_Pin) << 6
                  | HAL_GPIO_ReadPin(addr1_GPIO_Port, addr1_Pin) << 5
                  | HAL_GPIO_ReadPin(addr2_GPIO_Port, addr2_Pin) << 4
                  | HAL_GPIO_ReadPin(addr3_GPIO_Port, addr3_Pin) << 3
                  | HAL_GPIO_ReadPin(addr4_GPIO_Port, addr4_Pin) << 2
                  | HAL_GPIO_ReadPin(addr5_GPIO_Port, addr5_Pin) << 1
                  | HAL_GPIO_ReadPin(addr6_GPIO_Port, addr6_Pin) << 0);

    if (DITransmitQueueHandle == NULL) {
        DITransmitQueueHandle = xQueueCreate(10, sizeof(diTransmitQueueEntry_t));
    }

    DOTargetValue = 0;
    DORangeLow = 0;
    DORangeHigh = 100;
    
    Emil_StartTask();
    CreateTarget_StartTask();
    
    DI_InitTask();
    DI_StartTask();

    StepMotor_StartTask();

/* code */
    //postman_StartTask();

 /*   while (true) {
        HAL_GPIO_WritePin(DO1_GPIO_Port, DO1_Pin, GPIO_PIN_SET);
        vTaskDelay(5 * duration_ticks());

        HAL_GPIO_WritePin(DO1_GPIO_Port, DO1_Pin, GPIO_PIN_RESET);
        vTaskDelay(1 * duration_ticks());

        HAL_GPIO_WritePin(DO3_GPIO_Port, DO3_Pin, GPIO_PIN_SET);
        vTaskDelay(10 * duration_ticks());

        HAL_GPIO_WritePin(DO3_GPIO_Port, DO3_Pin, GPIO_PIN_RESET);
        vTaskDelay(30 * duration_ticks());
    } */
}
