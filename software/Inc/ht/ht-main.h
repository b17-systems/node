#ifndef __ht_main_H
#define __ht_main_H

#include "main.h"
#include "stdbool.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "timers.h"
#include "queue.h"

void startMainTask(void);
void stopMainTask(void);
void startTask(TaskHandle_t *taskHandle, TaskFunction_t taskFunction, const char* taskName, uint16_t stackDepth, UBaseType_t priority);
void stopTask(TaskHandle_t *taskHandle);

typedef struct {
    GPIO_TypeDef * port;
    uint16_t pin;
} portPin_t;

typedef uint8_t Address_t;
Address_t deviceAddress;

uint8_t DOTargetValue;
bool DOSynch;
uint8_t DORangeLow;
uint8_t DORangeHigh;

#endif /* __ht_main_H */