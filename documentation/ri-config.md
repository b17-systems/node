# Auswertung Widerstandseingänge

Der RI-Multiplexer kann entweder an die analoge Auswerteschaltung oder an das Digital-IC _MAX31865_ angeschlossen werden, beides gleichzeitig ist jedoch nicht umsetzbar.
Die Konfiguration der Ein-/Ausgänge am Prozessor muss entsprechend angepasst werden.

Pin | analog | digital
--- | ------ | -------
`PA4` | `RI` (unverstärkt) | `¬CS`
`PA5` | `RIX` (verstärkt)  | `SCK`
`PA6` | | `MISO`
`PA7` | `VREF-EN` | `MOSI`
`PC4` | | `¬DRDY`
