# Bestückungsvarianten

Von der Platine in der Version [v1.0](/../tags/v1.0-hardware) sind drei Varianten bestückt.
Jedes Exemplar ist mit einer Variantenbezeichnung **I**, **II** oder **III** beschriftet.

## Signalaufbereitung RIs

Alle Varianten haben gemeinsam:

|                    | **I** - **III** |
|:------------------ |:---------------:|
| Speisespannung _VREF_ | 2.5 V |
| Vorwiderstand _R56_ | 1.02 kΩ |
| Vorwiderstand _R52_ | 3.92 kΩ |

Die Verstärkerschaltung für die analoge Widerstandsmessung ist auf den verschiedenen Varianten unteschiedlich bestückt:

|                     | **I** | **II** | **III** |
|:------------------- |:-----:|:------:|:-------:|
| Temperaturbereich   | -25..150 °C | -20..40 °C | 10..50 °C |
| Verstärkung $`G_1`$ | 9.06 | 20.6 | 33.4 |
| Empfindlichkeit     | 30..15 LSB/K | 60 LSB/K | >80 LSB/K |
| $`R_1`$ (_R49_)     | 46.4 kΩ | 51.1 kΩ | 46.4 kΩ |
| $`R_2`$ (_R50_)     | 52.3 kΩ | 49.9 kΩ | 51.1 kΩ |
| $`R_3`$ (_R55_)     | 88.7 kΩ | 88.7 kΩ | 105 kΩ |
| $`R_4`$ (_R54_)     | 11.0 kΩ | 4.22 kΩ | 3.24 kΩ |

## Modifikationen

Auf Platine **I** ist der Schmitttrigger _U5_ entfernt, die Eingänge DI5..8 sind stattdessen direkt mit dem Prozessor verbunden. Die passiven Tiefpassfilter sind unverändert und weiterhin in Betrieb.
Achtung: durch das Wegfallen der Schmitttrigger haben die Eingänge nun umgedrehte Polarität!
